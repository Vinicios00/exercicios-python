
#print("EXERCICIO 1")

#num1 = int(input(f"Digite o primeiro numero:"))
#num2 = int(input(f"Digite o segundo numero:"))
#print()

#if num1 > num2:
#    print(f"O {num1} é maior que {num2}")
#elif num1 == num2:
#    print("Os dois numeros são iguais")
#else:
#    print(f"O {num2} é maior que {num1}")

#print()
#print("EXERCICIO 2")

#mes_anterior = float(input("Digite a quantidade de produção no mês anterior: "))
#mes_atual = float(input("Digite a quantidade de produção no mês atual:"))
#print()

#if mes_atual > mes_anterior:
#    crecimento = ((mes_atual - mes_anterior) / mes_anterior)*100
#    print(f"Sua empresa teve um crecimento de {crecimento}%")
#elif mes_atual == mes_anterior:
#    print("Sua empresa manteve o padrão do mês anterior")
#else:
#    crecimento = ((mes_atual - mes_anterior) / mes_anterior)*100
#    print(f"Sua empresa teve um decrecimento de {crecimento}%")


print("EXERCICIO 3")

x = int(input("Digite o primeiro numero:"))
y = int(input("Digite o segundo numero:"))

if x > y:
    for cont in range(y+1,x):
        print(cont)
else:
    for cont in range(x+1, y):
        print(cont)



print("EXERCICIO 4")

numero = int(input("Digite um número inteiro de 1 a 10: "))

if numero < 1 or numero > 10:
    print("Número inválido. Por favor, insira um número entre 1 e 10.")
else:
    print(f"Tabuada do {numero}:")

for i in range(1, 11):
    resultado = numero * i
    print(f"{numero} x {i} = {resultado}")

