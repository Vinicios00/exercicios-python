carros =[
    {"marca": "Sandeiro", "modelo": "Sandeiro", "preço": 35.000},
    {"marca": "Ferrari", "modelo": "250 GTO", "preço": 400.000},
    {"marca": "Toyota", "modelo": "Corola", "preço": 120.000}
]

while True:
    print("1 - compra")
    print("2 - cadastro")
    print("3 - listagem de carros")

    choice = int(input("Digite a opção:"))

    if choice == 3:
        for idk, carro in enumerate(carros):
            print(f"{idk + 1} - {carro['marca']} {carro['modelo']} (Preço: R$ {carro['preço']})")
            print()


    elif choice == 2:
        aux = int(input("Digite quantos carros quer cadastrar: "))

        for count in range(1, aux + 1):
            marca = input("Digite a marca do carro:")
            modelo = input("Digite o modelo do carro:")
            preco = input("Digite o preço do carro:")

            carros.append({"marca": marca, "modelo": modelo, "preço": preco})
            print()
            print("Carros cadastrados com sucesso!")
            print()
            for idd, carro in enumerate(carros):
                print(f"{idd + 1} - {carro['marca']} {carro['modelo']} (Preço: R$ {carro['preço']})")
                print()


    elif choice == 1:
        print("Lista de carros disponíveis:")
        for idx, carro in enumerate(carros):
            print(f"{idx + 1} - {carro['marca']} {carro['modelo']} (Preço: R$ {carro['preço']})")

        opcao_excluir = int(input("Digite o número do carro que deseja comprar: "))
        if 0 < opcao_excluir <= len(carros):
            del carros[opcao_excluir - 1]
            print("Carro comprado com sucesso!")
        else:
            print("Opção inválida! Por favor, digite um número de carro válido.")

    else:
        print("Opção errada, escolha novamente!")