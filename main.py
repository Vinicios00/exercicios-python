#nome = input('Qual o seu nome? ')
#print(nome)
#print(type(nome))

#ano_de_entrada = int(input('Qual o ano de entrada na faculdade ? '))
#print(ano_de_entrada)
#print(type(ano_de_entrada))

#nota = float(input('Digite a nota do estudante: '))
#print(nota)
#print(type(nota))

#nome = input(f'Qual o seu nome? ')
#print(f'Prazer em te conhecer, {nome}')

#media = float(input("Digite a sua média: "))

#if media >= 6.0:
#    print("Aprovado")
#elif media < 6.0:
#    print("Reprovado")


#cont = 1

#while cont <= 3:
#    nota_1 = float(input("Digite a primeira nota: "))
#    nota_2 = float(input("Digite a segunda nota: "))

#    print(f"Média foi {(nota_1+nota_2)/2}")

#    cont += 1


#for cont in range(1,11):
#    print(cont)

#lista = ["João", 9.5, 9.0, 8.0, True]


#for elemento in lista:
#    print(elemento)

#lista[3] = 10

#for elemento in lista:
#    print(elemento)


#media = (lista[1] + lista[2] + lista[3])/3
#print(f"Média: {media}")
#print(len(lista))


#lista.append(media)
#print(lista)

#lista.extend([10.0, 8.0, 7.0, 5.0])
#print(lista)

#lista.remove(10.0)

#dicionario = {
#    "chave_1":1,
#    "chave_2":2,
#}
#print(dicionario)

cadastro = {
    "matricula": 353143,
    "dia_cadastro": 21,
    "mes_cadastro": 9,
    "turma": "2E",
    "modalidade": "Presencial"
}

#print(cadastro["matricula"])
#print(cadastro["turma"])

# alterando valores das chaves
#cadastro["turma"] = "2G"
#cadastro["modalidade"] = "EAD"
#print(cadastro["turma"])
#print(cadastro["modalidade"])


# pop -> retira a chave selecionada
#cadastro.pop("turma")
#print(cadastro)

#print(cadastro.items())
#print(cadastro.keys())
#print(cadastro.values())

#for valores in cadastro.values():
#    print(valores)


#for chaves, valores in cadastro.items():
#    print(chaves, valores)


#def media():
#    num1 = float(input("Digita a nota 1:"))
#    num2 = float(input("Digita a nota 2:"))

#    print(f"Media: {(num1+num2)/2}")

#while True:
#    print("1 - CALCULAR MÉDIA")
#    print("2 - SAIR")

#    choice = int(input("Digite a opção:"))

#    if choice == 1:
#        media()
#    elif choice == 2:
#        break
#    else:
#        print("Opção inválida")


lista = [
    {"aluno": "Davi", "nota_1": 10, "nota_2": 8},
    {"aluno": "Leandro", "nota_1": 7, "nota_2": 9},
]
print(lista)

aux = int(input("Digite quantos alunos possuí: "))

for count in range(1, aux + 1):
    aluno = input("Digite o nome do aluno:")
    nota1 = input("Digite o primeiro nota:")
    nota2 = input("Digite o segundo nota:")

    lista.append({"aluno": aluno, "nota_1": nota1, "nota_2": nota2})
    print()
    print("Aluno adicionado com sucesso!")
    print(lista)
